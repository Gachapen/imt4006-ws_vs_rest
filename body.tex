Web services are software services that can be accessed on an end-point over the Internet.
A consumer and a producer of a service issue requests and send responses by passing messages between each other.
There are various standards and methods describing how services communicate.
Two of the commonly used methods are \say{Big} Web Services (WS-*) and RESTful Web Services (REST).

Before we dive into the differences between WS-* and REST, I would like to clarify that this essay will concern the common understanding of REST, which is also how Pautasso et al.~understands it~\cite{Pautasso08}.
Contrary to common understanding, a truly RESTful Web Service is independent of communication protocol and is more constrained, as clarified by its original author~\cite{Fielding2008}.
Bleigh discusses this issue and what benefits and drawbacks there are with truly RESTful Web Services~\cite{Bleigh2010}.

The World Wide Web Consortium (W3C) has standardized a set of specifications for \say{Big} Web services.
These specifications include SOAP, WSDL and more.
SOAP is an XML language that defines how messages passed between consumers and producers look.
WSDL is an XML language that defines interfaces to services.
A consumer would request the WSDL of a service to understand what interfaces exists and how they look and then communicate with these interfaces over SOAP.
WSDL can be used to generate the interfaces in a programming language so that developers of consumers can use the interfaces natively in the code without thinking about how the requests are passed over the Internet.~\cite{Pautasso08}

By using WS-*, the user has protocol transparency and independence.
This means that the Web Service can work over any transportation protocol that supports sending XML documents, not only HTTP.
This allows messages to be transported across various middleware systems that might use different transportation protocols.
The underlaying transportation protocol might even change during the transportation of a single message.

The transportation protocol is not the only part being abstracted by WS-*.
Serialization details and the platform is also abstracted.
WS-* is not tied to a certain operating system or programming language, thanks to WSDL.

One weakness of WS-* is that there can be leakage between the abstraction layers.
The leakage might be caused by the implementation exposing platform specific constructs in its WSDL interfaces.
This can cause interoperability issues.

Another weakness is that WS-* has a perceived complexity.
With several different standards and complex WSDL and SOAP documents, potential users may be scared of using it.
This is actually a false weakness.
The complexity is only perceived.
Because WS-* have been around for a long time, mature tools can hide the perceived complexity, thus making this a strength in stead.

WS-* specifications such as SOAP and WSDL also only support XML.
This can cause performance issues for marshalling and locks users into a specific format.

REST is a much simpler method of communicating with services.
In stead of relying on several complex standards, it relies on the already existing and well established HTTP protocol and some additional principles.
It is important to note that REST is not a standard, but rather a set of principles for Web Services.
The first principle of REST is that resources are identified by a URI.
Consumers communicate with the provider by passing requests to these URIs.
The second principle is that it has a uniform interface.
There are four HTTP methods for the interface: POST, PUT, DELETE and GET.
The third principle is that the messages are self-descriptive.
This means that the resources are decoupled from their representation.
Resources can be accessed and updated in various formats, e.g.~JSON, XML and HTML.
The fourth and last principle is that communication is stateless.
The service does not store any state information about the client, so each message must include all information necessary to service the request.

The most clear strength of REST is its simplicity.
By relying heavily on a well established protocol such as HTTP, it is easy to use and test.
A REST service can easily be tested in a web browser by typing the URL to a resource, or it can be tested by a command line program such as curl.
It scales well because of the caching, clustering and load balancing that is built into it.
In addition it can be optimized for performance because of its support of lightweight message formats such as JSON or plain text.

The weakness with REST is that there is confusion regarding best practices to use.
There are several best practices, or recommendations, such as ~Hi-REST and Lo-REST.
These recommendations advocate different ways of using REST, for example by limiting the HTTP methods to only GET and POST.

Another weakness is that it is impossible to strictly follow the GET vs. POST rule.
This is because servers often limit the size of the URI, resulting into requests with many or big parameters might be flagged as malformed and not handled.

Overall, REST and WS-* have different, but still somewhat similar, architectures.
They both have their unique strengths and weaknesses.
REST will be simpler to test and it scales well, but has confusing best practices and is limited because of the GET URL parameter size.
WS-* provides protocol transparency and independence, and abstracts serialization details and platforms, but it can also have a leakage in the abstraction layers.
While REST is commonly perceived as simpler than WS-* because of a simpler specification, WS-* with the right tools provide for more efficient development than REST.
As Pautasso et al.~concluded, WS-* should be used for \say{professional enterprise application integration} where the lifespan is longer and advanced QoS is required, while REST should be used for \say{tactical, ad hoc integration over the Web}~\cite{Pautasso08}.
